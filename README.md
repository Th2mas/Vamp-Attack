VampAttack

Ein Spiel in dem es darum geht, dass der Vampir solange wie möglich überlebt.
Um ins nächste Level zu gelangen, muss er eine bestimmte Anzahl an Herzen sammeln.
Dabei muss muss man den Knoblauchknollen ausweichen, ansonsten hat man verloren.
Weiters darf man nicht auf den unteren Rand fallen, ansonsten hat man ebenfalls verloren.

Die Anzahl der zu sammelnden Herzen befindet sich links oben.

Die Levelanzeige befindet sich in der Mitte oben.

Der Timer befindet sich rechts oben.

Der Punktestand befindet sich auf der unteren Leiste.

Um das Spiel zu spielen, einfach die .apk Datei auf das Handy laden
und installieren.
