package com.khlebovitch.vampattack;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.VideoView;

/**
 * Displays introduction video <br/>
 * Starting point <br/>
 */
public class VideoActivity extends AppCompatActivity {

    /**
     * Create a new Activity, set the content view to activity_video
     * @param savedInstanceState undefined
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        VideoView videoView = (VideoView)findViewById(R.id.videoview);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.vampattack_intro);
        videoView.setVideoURI(uri);
        videoView.start();

        final VideoActivity video = this;
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                startActivity(new Intent(video, MenuActivity.class));
            }
        });

    }

    /**
     * Skips the video
     * Goes to menu
     * @param view  The view calling this method
     */
    public void skipButtonClicked(View view){
        Intent skipIntent = new Intent(this, MenuActivity.class);
        startActivity(skipIntent);
    }


}
