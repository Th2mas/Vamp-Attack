package com.khlebovitch.vampattack;

import android.graphics.Bitmap;

/**
 * Creates an animation in the game
 */
public class Animation {

    /**
     * An array of frames, which will be animated
     */
    private Bitmap[] frames;

    /**
     * The number of the current frame
     */
    private int currentFrame;

    /**
     * Start time of the current frame in nanoseconds
     */
    private long startTime;

    /**
     * A delay in nanoseconds, which will be used for controlling
     * If the current time is greater than the delay, then the next frame's animation will be started
     */
    private long delay;

    /**
     * Boolean which says if this animation was played once
     */
    private boolean playedOnce;

    /**
     * Creates a new Animation with its given frames
     * @param frames to be animated
     */
    public Animation(Bitmap[] frames){
        this.frames = frames;
        startTime = System.nanoTime();
        currentFrame = 0;
    }

    /**
     * Updates the animation
     */
    public void update(){
        // Control unit for checking if it's already the next frame's turn to be animated
        long elapsed = (System.nanoTime() + startTime) / 1000000;

        if(elapsed >delay){
            currentFrame++;
            startTime = System.nanoTime();
        }

        if(currentFrame == frames.length){
            currentFrame = 0;
            playedOnce = true;
        }
    }

    /**
     * Returns the current frame of this animation
     * @return frames[currentFrame]
     */
    public Bitmap getFrame(){return frames[currentFrame];}

    /**
     * Sets a new delay in nanoseconds
     * @param delay new delay
     */
    public void setDelay(long delay){this.delay = delay;}

    /**
     * Checks if this animation was already played once, or not
     * @return  true, if animation was played completely; false, otherwise
     */
    public boolean isPlayedOnce(){return playedOnce;}
}