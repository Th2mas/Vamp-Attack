package com.khlebovitch.vampattack;

import android.graphics.Rect;

/**
 * A GameObject with its proper qualities
 * TODO: Move me to dto
 *
 */
public abstract class GameObject {

    /**
     * The GameObject's Rectangle start x position
     */
    protected int x;

    /**
     * The GameObject's Rectangle start y position
     */
    protected int y;

    /**
     * The GameObject's Rectangle width
     */
    protected int width;

    /**
     * The GameObject's Rectangle height
     */
    protected int height;

    /**
     * The GameObject's Rectangle slope in x direction
     */
    int dx;

    /**
     * The GameObject's Rectangle slope in y direction
     */
    int dy;

    /**
     * Set a new start x position for this GameObject's Rectangle
     * @param x new start x position
     */
    public void setX(int x)
    {
        this.x = x;
    }

    /**
     * Set a new start y position for this GameObject's Rectangle
     * @param y new start y position
     */
    public void setY(int y)
    {
        this.y = y;
    }

    /**
     * Get the current start x position of this GameObject's Rectangle
     * @return this.x
     */
    public int getX()
    {
        return x;
    }

    /**
     * Get the current start y position of this GameObject's Rectangle
     * @return this.y
     */
    public int getY()
    {
        return y;
    }

    /**
     * Get the current height of this GameObject's Rectangle
     * @return this.height
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * Get the current width of this GameObject's Rectangle
     * @return this.width
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Returns a new Rectangle of the current position of this GameObject's Rectangle
     * @return new Rect with the proper coordinates
     */
    public Rect getRectangle()
    {
        return new Rect(x, y, x+width, y+height);
    }
}