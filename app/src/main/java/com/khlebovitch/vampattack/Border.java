package com.khlebovitch.vampattack;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * The top border of the background image
 */
public class Border extends GameObject{

    /**
     * The actual image of the top border
     */
    private Bitmap image;

    /**
     * Creates a new image border
     * @param image     the spritesheet source
     * @param x         x start value
     * @param y         y start value
     * @param height    height of the image image
     */
    public Border(Bitmap image, int x, int y, int height, int width) {
        this.height = height;
        this.width = width;

        this.x = x;
        this.y = y;

        dx = GameView.MOVESPEED;
        this.image = Bitmap.createBitmap(image, 0, 0, this.width, this.height);
    }

    /**
     * Update the x value
     */
    public void update()
    {
        x+=dx;
    }

    /**
     * Draws the image on the given canvas
     * @param canvas Canvas to draw on
     */
    public void draw(Canvas canvas) {canvas.drawBitmap(image,x,y,null);}

}
