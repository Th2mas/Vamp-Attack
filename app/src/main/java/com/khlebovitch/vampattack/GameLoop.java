package com.khlebovitch.vampattack;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * The GameLoopGameView in which the game will take place
 *
 *
 */
public class GameLoop extends Thread {

    /**
     * This object's SurfaceHolder
     */
    private final SurfaceHolder surfaceHolder;

    /**
     * The Gameview this GameLoopGameView belongs to
     */
    private GameView gameView;

    /**
     * Frames per second
     */
    private int FPS;

    /**
     * Says if this GameLoopGameView is running
     */
    private boolean running;

    /**
     * Creates a new GameLoopGameView with the given parameters
     *
     * @param surfaceHolder this object's surfaceHolder
     * @param gameView      the GameView this GameLoopGameView belongs to
     */
    public GameLoop(SurfaceHolder surfaceHolder, GameView gameView) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.gameView = gameView;
        this.FPS = 30;
    }

    /**
     * Runs the GameLoopGameView with all its game logic
     */
    @Override
    public void run() {
        long startTime;
        long timeMillis;
        long waitTime;
        int frameCount = 0;
        long targetTime = 1000 / FPS;

        while (running) {
            startTime = System.nanoTime();
            Canvas canvas = null;

            //try locking the canvas for pixel editing
            try {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    this.gameView.update();
                    this.gameView.draw(canvas);
                }
            } catch (Exception e) {
            } finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            timeMillis = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - timeMillis;

            try {
                sleep(waitTime);
            } catch (Exception e) {
            }

            frameCount++;
            if (frameCount == FPS)
                frameCount = 0;
        }
    }

    /**
     * Sets {@link #running} to the given parameter
     *
     * @param running the new running value
     */
    public void setRunning(boolean running) {
        this.running = running;
    }

}