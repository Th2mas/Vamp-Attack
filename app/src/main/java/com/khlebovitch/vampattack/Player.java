package com.khlebovitch.vampattack;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * A Player object, which will be controlled by the actual player
 * TODO: Move to dto
 *
 */
public class Player extends GameObject{

    /**
     * An Animation, which will animate the Player
     */
    private Animation animation;

    /**
     * Is the Player jumping, or not
     *
     */
    private boolean up;

    /**
     * Is the Player playing, or not
     */
    private boolean playing;

    /**
     * This player's score
     */
    private int score;

    /**
     * This player's starttime
     */
    private long startTime;

    /*
    -------------------------------------------
    */

    /**
     * Creates a new Player with the given spritesheet and default start values
     * @param spritesheet       a Bitmap with the Player's moves / frames on it
     * @param width             the width of a Player's frame
     * @param height            the height of a Player's frame
     * @param numberOfFrames    the number of frames in the Bitmap
     */
    public Player(Bitmap spritesheet, int width, int height, int numberOfFrames){
        this(spritesheet,
                100,
                GameView.HEIGHT/2,
                width,
                height,
                numberOfFrames);
    }

    /**
     * Creates a new Player with the given spritesheet
     * @param spritesheet       a Bitmap with the Player's moves / frames on it
     * @param x                 the start x value of the Player's Rectangle
     * @param y                 the start y value of the Player's Rectangle
     * @param width             the width of a Player's frame
     * @param height            the height of a Player's frame
     * @param numberOfFrames    the number of frames in the Bitmap
     */
    public Player(Bitmap spritesheet, int x, int y, int width, int height, int numberOfFrames){
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        score = 0;
        dy = 0;

        Bitmap[] frames = new Bitmap[numberOfFrames];

        // Split the spritesheet into its frames
        for(int i=0; i<frames.length; i++){
            frames[i] = Bitmap.createBitmap(spritesheet,i*width,0,width,height);
        }

        animation = new Animation(frames);
        animation.setDelay(10);

        startTime = System.nanoTime();
    }

    /**
     * Update the Player's position
     */
    public void update(){

        long elapsed = (System.nanoTime()-startTime)/1000000;

        // updateSurroundings the score
        if(elapsed>100) {
            score++;
            startTime = System.nanoTime();
        }

        animation.update();

        if(up)
            dy--;
        else
            dy++;

        // Check if it's not out of bounds
        if(dy>14) dy=14;
        if(dy<-14) dy = -14;

        y += dy*2;
    }

    /**
     * Sets this.up to the given parameter
     * @param up the new {@link #up} value
     */
    public void setUp(boolean up){this.up = up;}

    /**
     * Draws this Player to the canvas
     * @param canvas the canvas the Player will be drawn on
     */
    public void draw(Canvas canvas){canvas.drawBitmap(animation.getFrame(),x,y,null);}

    /**
     * Sets the current {@link #playing} value to the given parameter
     * @param playing new value for playing
     */
    public void setPlaying(boolean playing){this.playing = playing;}

    /**
     * Returns the current {@link #playing} values
     * @return this.playing
     */
    public boolean getPlaying(){return this.playing;}

    /**
     * Resets the current slope in y direction
     */
    public void resetDY(){this.dy = 0;}

    /**
     * Reset {@link #score}
     */
    public void resetScore(){score = 0;}

    /**
     * Get this player's score
     * @return this.score
     */
    public int getScore(){return score;}
}
