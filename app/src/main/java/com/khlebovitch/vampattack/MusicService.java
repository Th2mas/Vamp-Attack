package com.khlebovitch.vampattack;

import android.media.MediaPlayer;

/**
 * Creates a MusicService, which handles MediaPlayer and its functions
 *
 *
 */
public class MusicService {

    /**
     * The MusicService's media player
     */
    private MediaPlayer mediaPlayer;

    /**
     * A new MusicService with the given GameActivity will be started
     * @param gameActivity  the GameActivity this MusicService belongs to
     */
    public MusicService(GameActivity gameActivity){
        this.mediaPlayer = MediaPlayer.create(gameActivity,R.raw.vampattack_reverb);
        mediaPlayer.setLooping(true);
        start();
    }

    /**
     * Starts the music
     */
    public void start(){
        this.mediaPlayer.start();
    }

    /**
     * Pauses the music
     */
    public void pause(){
        this.mediaPlayer.pause();
    }

    /**
     * Stops the music
     */
    public void stop(){
        this.mediaPlayer.stop();
    }
}
