package com.khlebovitch.vampattack;
import android.os.Handler;

/**
 * A runnable object for counting the {@link #countdown} down
 * TODO: Create a real countdown, which ends the game, if the counter is 0
 * TODO: Maybe not through a runnable, but with a counter in the GameLoop?
 */
public class Countdown implements Runnable {

    /**
     * The GameActivity this object belongs to
     */
    private GameActivity gameActivity;

    /**
     * Creates a new countdown
     * @param countdown the new countdown
     */
    public Countdown(int countdown, GameActivity gameActivity){
        setCountdown(countdown);
        this.gameActivity = gameActivity;
    }

    /**
     * Time the character has left to pass the level in seconds
     */
    private int countdown;

    /**
     * Handles Runnables in this class
     */
    private Handler handler = new Handler();

    /**
     * Runs the countdown
     */
    @Override
    public void run() {
        if(!gameActivity.getPauseGame())
            count();
        if (countdown <= 0) {
            gameActivity.setLost(true);
        } else {
            handler.postDelayed(this, 1000 - gameActivity.getLevel().getLevel() * 50);
        }
    }

    /**
     * Sets the parameter to the current countdown
     * @param countdown new countdown
     */
    public void setCountdown(int countdown){this.countdown = countdown;}

    /**
     * Retuns this {@link #countdown}
     * @return this.countdown
     */
    public int getCountdown(){return this.countdown;}

    /**
     * Decrements this {@link #countdown}
     */
    public void count(){countdown--;}
}
