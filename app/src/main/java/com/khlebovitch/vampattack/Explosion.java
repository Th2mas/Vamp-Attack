package com.khlebovitch.vampattack;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Creates a new explosion with animation
 * TODO: Create a custom explosion (or a simple particle effect)
 */
public class Explosion {

    /**
     * The x coordinate of the explosion
     */
    private int x;

    /**
     * The y coordinate of the explosion
     */
    private int y;

    /**
     * The height of the explosion
     */
    private int height;

    /**
     * The row of the explosion
     */
    private int row;

    /**
     * The Animation, that will animate the Explosion
     */
    private Animation animation;

    /**
     * Create a new Explosion with the given spritesheet
     * @param spritesheet   the spritesheet Bitmap to be used for the explostion
     * @param x             the x coordinate of the explosion
     * @param y             the y coordinate of the explosion
     * @param width         the width of the explosion
     * @param height        the height of the explosion
     * @param numFrames     the number of frames, that will be used to draw the explosion
     */
    public Explosion(Bitmap spritesheet, int x, int y, int width, int height, int numFrames) {
        this.x = x;
        this.y = y;
        this.height = height;

        Bitmap[] image = new Bitmap[numFrames];

        for(int i = 0; i<image.length; i++)
        {
            if(i%5==0&&i>0)row++;
            image[i] = Bitmap.createBitmap(spritesheet, (i-(5*row))* width, row* this.height, width, this.height);
        }
        //animation.setFrames(image);
        animation = new Animation(image);
        animation.setDelay(10);

    }

    /**
     * Draw the explosion on the given canvas
     * @param canvas    the {@link Canvas} to be drawn on
     */
    public void draw(Canvas canvas) {

        if(!animation.isPlayedOnce())
            canvas.drawBitmap(animation.getFrame(),x,y,null);

    }

    /**
     * Update the animation of the explosion
     */
    public void update() {

        if(!animation.isPlayedOnce())
            animation.update();
    }

    /**
     * Get the height of the explosion
     * @return this.height
     */
    public int getHeight(){return height;}
}