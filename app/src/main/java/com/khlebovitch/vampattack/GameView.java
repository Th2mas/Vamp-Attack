package com.khlebovitch.vampattack;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

/**
 * Main SurfaceView, which displays the game <br/>
 *
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    /**
     * The current player
     *
     */
    private Player player;

    /**
     * The current background
     *
     */
    private Background background;

    /**
     * The current GameLoop
     *
     */
    private GameLoop gameLoop;

    /**
     * The static width of the background image
     *
     */
    public static final int WIDTH = 860;

    /**
     * The static height of the background image
     *
     */
    public static final int HEIGHT = 645;

    /**
     * Current level
     *
     */
    private Level level;

    /**
     * Hearts the Player has already collected
     *
     */
    private int collectedHearts;

    /**
     * The {@link GameActivity} this GameView belongs to
     *
     */
    private GameActivity gameActivity;

    /**
     * ID of the bottom border image
     *
     */
    private int bottomId;

    /**
     * ID of the opponent image
     *
     */
    private int opponentId;

    /**
     * ID of the heart image
     *
     */
    private int heartId;

    /**
     * The width of the bottom Border
     *
     */
    private int bottomBorderWidth;

    /**
     * The height of the bottom Border
     *
     */
    private int bottomBorderHeight;

    /**
     * Movespeed of the opponents
     *
     */
    public static final int MOVESPEED = -5;

    /**
     * Start time of the obstacles in nanoseconds
     *
     */
    private long obstacleStartTime;

    /**
     * {@link ArrayList} of opponents
     *
     */
    private ArrayList<Obstacle> opponents;

    /**
     * {@link ArrayList} of hearts
     *
     */
    private ArrayList<Obstacle> hearts;

    /**
     * {@link ArrayList} of bottom Borders
     *
     */
    private ArrayList<Border> bottomBorder;

    /**
     * {@link Random} object for further calculations
     *
     */
    private Random rand = new Random();

    /**
     * Maximum height of the bottom Border
     *
     */
    private int maxBorderHeight;

    /**
     * Boolean if a new game was created
     *
     */
    private boolean newGameCreated;

    /**
     * This GameView's Explosion animation
     *
     */
    private Explosion explosion;

    /**
     * Start time of reset in nano seconds
     *
     */
    private long startReset;

    /**
     * Tells if game was reset
     *
     */
    private boolean reset;

    /**
     * Tells if player has to disappear
     *
     */
    private boolean disappear;

    /**
     * Tell if game has started
     *
     */
    private boolean started;

    /**
     * Best score so far
     *
     */
    private int best;

    /**
     * Boolean for setting another image to the border
     *
     */
    private boolean setBorder;

    /**
     * Tells if the game is not the first game
     * There is a difference between a game is completely newly started
     * or it is a new game after the Player lost
     *
     */
    private boolean isNotFirstGame;

    /**
     * Margin for further calculations
     *
     */
    private int margin = getResources().getDimensionPixelSize(R.dimen.margin);

    /**
     * Standard text size
     *
     */
    private int textSize = getResources().getDimensionPixelSize(R.dimen.text_size);

    /**
     * Standard tutorial title size
     *
     */
    private int titleSize = getResources().getDimensionPixelSize(R.dimen.game_title_size);

    /**
     * Standard tutorial text size
     *
     */
    private int fontSize = getResources().getDimensionPixelSize(R.dimen.font_size);

    /**
     * approximate center
     *
     */
    private int center = (WIDTH/2)-textSize*2;

    /**
     * approximate right side
     *
     */
    private int right = WIDTH-textSize*3;

    /**
     * approximate left side
     *
     */
    private int left = margin;

    /**
     * approximate top margin
     *
     */
    private int topMargin = textSize + margin;

    /**
     * approximate bottom margin
     *
     */
    private int bottomMargin = HEIGHT - margin;

    /**
     * The default paint
     *
     */
    private Paint paint = new Paint();

    /**
     * This objects canvas
     *
     */
    private Canvas c;

    /**
     * Sets the game to lost or still playing
     */
    private boolean lost;

    /**
     * Create a new GameView
     * @param context           this objects context - the GameActivity
     * @param attributeSet      the attributes, that were set in the activity_game.xml file
     *
     *
     */
    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        getHolder().addCallback(this);
        gameActivity = (GameActivity)context;
    }

    /**
     * Tells what happens, when this object is created
     * @param holder undefined
     *  and thomas
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        c = getHolder().lockCanvas();

        background = new Background(BitmapFactory.decodeResource(getResources(),R.drawable.background_castle));
        player = new Player(
                BitmapFactory.decodeResource(getResources(), R.drawable.vamp_big_spritesheet),
                58,
                72,
                4
        );

        // Create a new Level
        level = gameActivity.getLevel();

        // Create new opponents
        opponents = new ArrayList<>();

        // Create new hearts
        hearts = new ArrayList<>();

        opponentId = R.drawable.garlic_spritesheet;

        heartId = R.drawable.heart;

        // Create a new bottom border
        bottomBorder = new ArrayList<>();
        bottomBorderWidth = 150;
        bottomBorderHeight = 81;

        setBorder = false;

        lost = false;

        isNotFirstGame = false;

        bottomId = R.drawable.bottom_border;

        // Set the opponents start time
        obstacleStartTime = System.nanoTime();

        gameLoop = new GameLoop(getHolder(), this);
        gameLoop.setRunning(true);
        gameLoop.start();

        draw(c);
        getHolder().unlockCanvasAndPost(c);
    }

    /**
     * Do nothing if surface is changed
     * @param holder    undefined
     * @param format    undefined
     * @param width     undefined
     * @param height    undefined
     *
     */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

    /**
     * Cancel everything on surface destroyed
     * @param holder undefined
     *
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        int counter = 0;
        gameActivity.musicService.stop();
        while(retry && counter<1000)
        {
            counter++;
            try{
                gameLoop.setRunning(false);
                gameLoop.join();
                retry = false;
                gameLoop = null;

            } catch(InterruptedException e){e.printStackTrace();}

        }
    }

    /**
     * Tells you what should happen on any touch event
     * @param event the MotionEvent, that occured
     * @return true, if one of the specified MotionEvents occured; super.onTouchEvent(event) otherwise
     *
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch(event.getAction()){

            // Case: Touched the TouchPad
            case MotionEvent.ACTION_DOWN:
                if(!player.getPlaying() && newGameCreated && reset) {
                    player.setPlaying(true);
                    player.setUp(true);
                }

                if(player.getPlaying()) {

                    if(!started)
                        started = true;
                    reset = false;
                    player.setUp(true);
                }

                return true;

            // Case: Released the TouchPad
            case MotionEvent.ACTION_UP:
                player.setUp(false);
                return true;

            // Case: None of the above cases occured
            default:
                return super.onTouchEvent(event);
        }
    }

    /**
     * Updates this Gameview
     *  and thomas
     */
    public void update(){
        if(player.getPlaying()) {

            if(bottomBorder.isEmpty()) {
                player.setPlaying(false);
                return;
            }

            // if Player has collected as many hearts as he needs -> start a new level
            if(collectedHearts-this.level.heartsToCollect()==0){
                gameActivity.getLevel().setLevel(gameActivity.getLevel().getLevel()+1);
                newGame();
            }

            background.update();
            player.update();



            // Avoid a disappearing of the player
            if(player.getY()<0)
                player.setY(0);

            //calculate the threshold of height the border can have based on the score
            //max and min border heart are updated, and the border switched direction when either max or
            //min is met


            //Increase to slow down difficulty progression, decrease to speed up difficulty progression
            int progressDenom = 20;
            maxBorderHeight = 30+player.getScore()/ progressDenom;
            //cap max border height so that borders can only take up a total of 1/2 the screen
            if(maxBorderHeight > HEIGHT/4)
                maxBorderHeight = HEIGHT/4;

            //check bottom border collision

            for(int i = 0; i< bottomBorder.size(); i++) {

                if(collision(bottomBorder.get(i), player)) {
                    player.setPlaying(false);
                }
            }

            //udpate bottom border
            this.updateBottomBorder();

            // add opponents on timer
            long elapsed = (System.nanoTime()- obstacleStartTime)/1000000;

            if(elapsed >(2000 - player.getScore()/4)) {

                //first opponent always goes down the middle
                if(opponents.size()==0)
                    opponents.add(new Obstacle(
                            BitmapFactory.decodeResource(getResources(),opponentId),
                            WIDTH + 10,
                            HEIGHT/2,
                            36,
                            36,
                            player.getScore(),
                            8
                    ));
                else
                    opponents.add(new Obstacle(
                            BitmapFactory.decodeResource(getResources(),opponentId),
                            WIDTH+10,
                            (int)(rand.nextDouble()*(HEIGHT - (maxBorderHeight * 2))+maxBorderHeight),
                            36,
                            36,
                            player.getScore(),
                            8
                    ));

                //reset timer
                obstacleStartTime = System.nanoTime();
            }


            if(elapsed >(2000 - player.getScore()/4)) {

                //first heart always goes down the middle
                if(hearts.size()==0)
                    hearts.add(new Obstacle(
                            BitmapFactory.decodeResource(getResources(),heartId),
                            WIDTH + 100,
                            HEIGHT/2,
                            21,
                            19,
                            player.getScore(),
                            2
                    ));
                else
                    hearts.add(new Obstacle(
                            BitmapFactory.decodeResource(getResources(),heartId),
                            WIDTH + 30,
                            (int)(rand.nextDouble()*(HEIGHT - (maxBorderHeight * 2))+maxBorderHeight),
                            21,
                            19,
                            player.getScore(),
                            2
                    ));

                //reset timer
                obstacleStartTime = System.nanoTime();
            }


            //loop through every opponent and check collision and remove
            for(int i = 0; i< opponents.size(); i++) {

                //updateSurroundings opponent
                opponents.get(i).update();

                if(collision(opponents.get(i),player)) {
                    opponents.remove(i);
                    player.setPlaying(false);
                    break;
                }

                //remove opponent if it is way off the screen
                if(opponents.get(i).getX()<-100) {
                    opponents.remove(i);
                    break;
                }
            }

            //loop through every heart and check collision and remove
            for(int i = 0; i< hearts.size(); i++) {

                //updateSurroundings heart
                hearts.get(i).update();

                if(collision(hearts.get(i),player)) {
                    hearts.remove(i);
                    collectedHearts++;
                    break;
                }

                //remove opponent if it is way off the screen
                if(hearts.get(i).getX()<-100) {
                    hearts.remove(i);
                    break;
                }
            }
        }
        else {

            player.resetDY();
            if(!reset) {

                level = gameActivity.setNewLevel();
                collectedHearts = 0;

                newGameCreated = false;
                startReset = System.nanoTime();
                reset = true;
                disappear = true;
                explosion = new Explosion(
                        BitmapFactory.decodeResource(getResources(),R.drawable.explosion),
                        player.getX(),
                        player.getY()-30,
                        100,
                        100,
                        25
                );
            }

            explosion.update();
            long resetElapsed = (System.nanoTime()-startReset)/1000000;

            if(resetElapsed > 2500 && !newGameCreated)
                newGame();
        }
    }

    /**
     * Updates the bottom Border of the image
     *  and thomas
     */
    public void updateBottomBorder() {

        for(int i = 0; i< bottomBorder.size(); i++) {
            bottomBorder.get(i).update();

            if(!setBorder){
                bottomBorder.add(new Border(
                        BitmapFactory.decodeResource(getResources(), bottomId),
                        bottomBorder.get(bottomBorder.size()-1).getX()+bottomBorderWidth,
                        bottomBorder.get(bottomBorder.size()-1).getY(),
                        bottomBorderHeight,
                        bottomBorderWidth
                ));
                setBorder = true;
            }

            //if border is moving off screen, remove it and add a corresponding new one
            if(bottomBorder.get(i).getX()<-bottomBorderWidth) {
                bottomBorder.remove(i);


                bottomBorder.add(new Border(
                        BitmapFactory.decodeResource(getResources(), bottomId),
                        bottomBorder.get(bottomBorder.size()-1).getX()+bottomBorderWidth,
                        bottomBorder.get(bottomBorder.size()-1).getY(),
                        bottomBorderHeight,
                        bottomBorderWidth
                ));
                setBorder = false;
            }
        }
    }

    /**
     * Detects a collision
     * @param a first GameObject
     * @param b second GameObject
     * @return  true, if collided; false otherwise
     *
     */
    public boolean collision(GameObject a, GameObject b) {
        return Rect.intersects(a.getRectangle(), b.getRectangle());
    }

    /**
     * Draws all objects we need in our game onto this GameView
     * @param canvas the canvas that will be used for drawing
     *  and thomas
     */
    @Override
    public void draw(Canvas canvas) {

        final float scaleFactorX = getWidth()/(WIDTH*1.f);
        final float scaleFactorY = getHeight()/(HEIGHT*1.f);

        lost = false;

        if(canvas!=null) {
            final int savedState = canvas.save();
            canvas.scale(scaleFactorX, scaleFactorY);

            //draw background
            background.draw(canvas);

            //draw player
            if(!disappear)
                player.draw(canvas);

            //draw opponents
            for(Obstacle m: opponents)
                m.draw(canvas);

            //draw hearts
            for(Obstacle h: hearts)
                h.draw(canvas);

            //draw bottomBorder
            for(Border bb: bottomBorder)
                bb.draw(canvas);

            //draw explosion
            if(started){
                explosion.draw(canvas);
                lost = true;
            }

            /*
            if(lost){
                Animation viewAnimation = AnimationUtils.loadAnimation(gameActivity,R.anim.viewanimation);
                TextView textView = (TextView)gameActivity.findViewById(R.id.textview);
                textView.setText(getText(R.string.lost));
                textView.startAnimation(viewAnimation);
            }
            */

            //draw the surrounding texts
            drawSurroundings(canvas);

            canvas.restoreToCount(savedState);
        }
    }

    /**
     * Draws surroundings on the given canvas
     * @param canvas the {@link Canvas} the text will be drawn on
     *
     */
    public void drawSurroundings(Canvas canvas){
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        // Draw hearts left

        String heartsText = getText(R.string.hearts)+ (level.heartsToCollect()-collectedHearts);

        /*
      The standard text color
      */
        int textColor = Color.WHITE;
        drawText(
                canvas,
                heartsText,
                textSize,
                textColor,
                left,
                topMargin);

        // Draw level
        String levelText = level.toString();

        drawText(canvas,
                levelText,
                textSize,
                textColor,
                center,
                topMargin);

        // Draw countdown

        String countdownText = gameActivity.getCountdown().getCountdown()+"";

        drawText(canvas,
                countdownText,
                textSize,
                textColor,
                right,
                textSize+margin);

        // Draw distance
        drawText(canvas,
                getText(R.string.distance) + (player.getScore()*3),
                textSize,
                textColor,
                center,
                bottomMargin);

        // Draw highscore
        drawText(canvas,
                getText(R.string.highscore) + this.best,
                textSize,
                textColor,
                right-(getText(R.string.highscore)+this.best).length()*12,
                bottomMargin);

        // At start: draw Tutorial
        if(!player.getPlaying() && newGameCreated && reset) {
            paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

            int fontX = WIDTH/2-100;
            int fontCounter = 1;

            drawText(canvas,
                    getText(R.string.start),
                    titleSize,
                    textColor,
                    WIDTH/3,
                    HEIGHT/2);

            drawText(canvas,
                    getText(R.string.holdUp),
                    fontSize,
                    textColor,
                    fontX,
                    HEIGHT/2+fontCounter++*fontSize);

            drawText(canvas,
                    getText(R.string.holdDown),
                    fontSize,
                    textColor,
                    fontX,
                    HEIGHT/2+fontCounter++*fontSize);

            drawText(canvas,
                    getText(R.string.heartsTutorial),
                    fontSize,
                    textColor,
                    fontX,
                    HEIGHT/2+fontCounter++*fontSize);

            drawText(canvas,
                    getText(R.string.garlicTutorial),
                    fontSize,
                    textColor,
                    fontX,
                    HEIGHT/2+fontCounter*fontSize);
        }
    }

    /**
     * Draw the given text on the given canvas
     * @param canvas    Canvas to draw on
     * @param text      Text to be written
     * @param textSize  Textsize of the given text
     * @param textColor Textcolor of the given text
     * @param x         x-coordinate of the given text
     * @param y         y-coordinate of the given text
     *
     */
    public void drawText(Canvas canvas, String text, int textSize, int textColor, int x, int y){
        paint.setColor(textColor);
        paint.setTextSize(textSize);
        canvas.drawText(text,x,y,paint);
    }

    /**
     * Initiates a new game
     *  and thomas
     */
    public void newGame() {
        disappear = false;

        // Reset the Player's statistics, start a new level
        collectedHearts = 0;

        // Reset countdown
        gameActivity.getCountdown().setCountdown(gameActivity.getStartTime());

        // Reset music
        //gameActivity.musicService.restart();

        maxBorderHeight = 30;

        // Do only stuff, if game was created new -> not new level
        if(level.getLevel()==1) {
            player.resetDY();
            player.resetScore();
            player.setY(HEIGHT / 2);
            opponents.clear();
            hearts.clear();
            // Reset bottomBorder, if the game was started new; otherwise do nothing
            bottomBorder.clear();

        }

        // TODO: add highscore
        if(player.getScore()>best && this.lost)
            best = player.getScore();

        // initial bottom border
        for(int i = 0; i*bottomBorderWidth<WIDTH+40; i++) {
            bottomBorder.add(new Border(
                    BitmapFactory.decodeResource(getResources(), bottomId),
                    i * bottomBorderWidth,
                    HEIGHT - bottomBorderHeight,
                    bottomBorderHeight,
                    bottomBorderWidth
            ));
            // initial bottom border only if game was started new
            if(level.getLevel()>1)
                bottomBorder.remove(bottomBorder.size()-1);
        }

        if(isNotFirstGame){
            bottomBorder.add(new Border(
                    BitmapFactory.decodeResource(getResources(), bottomId),
                    bottomBorder.get(bottomBorder.size()-1).getX()+bottomBorderWidth,
                    bottomBorder.get(bottomBorder.size()-1).getY(),
                    bottomBorderHeight,
                    bottomBorderWidth
            ));
        }

        isNotFirstGame = true;
        newGameCreated = true;
    }

    /**
     * Returns the string from the R.string object with the given id
     * @param id    id of the requested String
     * @return  requested String
     *
     */
    public String getText(int id){
        return gameActivity.getText(id).toString();
    }

    /**
     * Returns this canvas
     * @return this.c
     *
     */
    public Canvas getCanvas(){return this.c;}
}