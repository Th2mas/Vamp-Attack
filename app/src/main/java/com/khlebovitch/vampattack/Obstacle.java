package com.khlebovitch.vampattack;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.Random;

/**
 * Creates a new opponent for the Player
 * TODO: Move to DTO
 */
public class Obstacle extends GameObject{

    /**
     * This Obstacle's speed
     */
    private int speed;

    /**
     * The {@link Animation} object, which will animate this object
     */
    private Animation animation;

    /**
     * Create a new opponent
     * @param spritesheet   the opponent's spritesheet
     * @param x             the x start value
     * @param y             the y start value
     * @param width         the width of this object
     * @param height        the height of this object
     * @param score         the score, which will be used for further calculations
     * @param numFrames     number of Frames of the spritesheet
     */
    public Obstacle(Bitmap spritesheet, int x, int y, int width, int height, int score, int numFrames) {
        super.x = x;
        super.y = y;
        this.width = width;
        this.height = height;

        Random rand = new Random();
        speed = 7 + (int) (rand.nextDouble()* score /30);

        //cap missile speed
        if(speed>40)speed = 40;

        Bitmap[] image = new Bitmap[numFrames];

        for(int i = 0; i<image.length;i++) {
            image[i] = Bitmap.createBitmap(spritesheet, 0, i* this.height, this.width, this.height);
        }

        animation = new Animation(image);
        animation.setDelay(100-speed);

    }

    /**
     * Updates this object
     */
    public void update() {
        x-=speed;
        animation.update();
    }

    /**
     * Draws this object on the given canvas
     * @param canvas the canvas this object will be drawn on
     */
    public void draw(Canvas canvas) {canvas.drawBitmap(animation.getFrame(),x,y,null);}

    @Override
    public int getWidth() {
        //offset slightly for more realistic collision detection
        return width-10;
    }

}
