package com.khlebovitch.vampattack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Displays menu
 *
 *
 *
 */
public class MenuActivity extends AppCompatActivity {

    /**
     * Sets features, loads data
     * @param savedInstanceState the saved states of the given Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    /**
     * Starts game activity
     * @param view the View that is calling this method
     */
    public void gameButtonClicked(View view){
        startActivity(new Intent(this, GameActivity.class));
    }

    /**
     * Starts highscore activity
     * @param view the View that is calling this method
     */
    public void highscoreButtonClicked(View view){
        startActivity(new Intent(this, HighscoreActivity.class));
    }

    /**
     * Closes application, shows home
     * @param view the View that is calling this method
     */
    public void exitButtonClicked(View view){
        finish();
    }
}
