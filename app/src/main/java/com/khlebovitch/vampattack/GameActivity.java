package com.khlebovitch.vampattack;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Creates a new GameActivity with OnClickListeners for a menu and sound button
 *
 *
 */
public class GameActivity extends AppCompatActivity {

    /**
     * Time the character needed to pass in seconds
     * Formula: startTime - countdown
     */
    private int time;

    /**
     * Starttime for every level in seconds
     */
    private final int startTime = 100;

    /**
     * The countdown for this object
     */
    private Countdown countdown;

    /**
     * The music for this object
     */
    public MusicService musicService;

    /**
     * Handles Runnables in this class
     */
    private Handler handler = new Handler();

    /**
     * Current level
     */
    private Level level;

    /**
     * Flag, that tells if the player died before he has finished the level
     */
    private boolean lost = false;

    /**
     * Flag, that tells if game was stopped or not
     */
    private boolean pauseGame = false;

    /**
     * Flag, that tells if sound was stopped or not
     */
    private boolean pauseSound;

    /**
     * {@link GameView}, that is drawn by this activity
     */
    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);

        android.view.animation.Animation viewAnimation = AnimationUtils.loadAnimation(this,R.anim.viewanimation);
        TextView textView = findViewById(R.id.textview);
        textView.setText(getText(R.string.app_name));
        textView.setTextColor(getResources().getColor(R.color.blood));
        textView.setTextSize(getResources().getDimensionPixelSize(R.dimen.fab_margin));
        textView.startAnimation(viewAnimation);
        textView.setVisibility(View.INVISIBLE);

        create();
    }

    /*
    ---------------------------------------------------------------------------------------
                                      Initialize
    ---------------------------------------------------------------------------------------
     */


    /**
     * Initiates a new game
     */
    private void create(){
        gameView = (GameView)findViewById(R.id.gameView);
        level = new Level(this);

        // Sets a new countdown
        countdown = new Countdown(startTime,this);

        // Start the music from the beginning
        pauseSound = false;
        musicService = new MusicService(this);

        // Set an onClickListener: Pause
        findViewById(R.id.stop_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {pauseGame();}
        });

        // Set an onClickListener: Sound
        findViewById(R.id.sound_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {pauseSound();}
        });

        initiateCountdown();
    }

    /**
     * Initiates a new level
     */
    private void initiateCountdown(){
        handler.postDelayed(countdown,1000-level.getLevel()*50);
    }

    /*
    ---------------------------------------------------------------------------------------
                                        Pause
    ---------------------------------------------------------------------------------------
     */

    /**
     * Stops the sound, changes the image from sound to mute
     */
    private void pauseSound() {
        pauseSound = !pauseSound;
        if(pauseSound)
            musicService.pause();
        else
            musicService.start();
    }

    /**
     * Stops the game, stops the countdown, shows the pauseGame menu (dialog_pause)
     */
    private void pauseGame() {

        // Pause Game
        setPauseGame(true);

        // Pauses the game in the gameView
        gameView.getHolder().lockCanvas();

        // Pauses Music
        musicService.pause();

        // Do dialog stuff

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.dialog_pause);

        dialog.findViewById(R.id.backToGame_dialogPause).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                returnToGame();
            }
        });

        dialog.findViewById(R.id.backToMenu_dialogPause).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                returnToMenu();
            }
        });

        dialog.findViewById(R.id.showTutorial_dialogPause).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showTutorial();
            }
        });

        dialog.show();
    }

    /*
    ---------------------------------------------------------------------------------------
                                      Return
    ---------------------------------------------------------------------------------------
     */

    /**
     * Returns to the game, starts the countdown again
     */
    private void returnToGame(){
        if(pauseSound)
            musicService.pause();
        else
            musicService.start();
        setPauseGame(false);
        gameView.getHolder().unlockCanvasAndPost(gameView.getCanvas());
    }

    /**
     * Exits the game, shows the MainActivity
     */
    private void returnToMenu(){
        musicService.stop();
        finish();
    }

    /*
    ---------------------------------------------------------------------------------------
                                        Show
    ---------------------------------------------------------------------------------------
     */

    /**
     * Shows the tutorial dialog
     */
    public void showTutorial(){
        final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.dialog_tutorial);

        dialog.findViewById(R.id.backToGame_dialogTutorial).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                returnToGame();
            }
        });

        dialog.show();
    }

    /*
    ---------------------------------------------------------------------------------------
                                         OnClickListener
    ---------------------------------------------------------------------------------------
     */

    /**
     * Starts a new game
     * @param view undefined
     */
    public void newGameButtonClicked(View view){
        ViewGroup container = findViewById(R.id.container_GameActivity);

        if(lost) container.removeView(findViewById(R.id.fragment_lost_container));
        else container.removeView(findViewById(R.id.fragment_won_container));

        create();
    }

    /**
     * Returns to menu
     * @param view undefined
     */
    public void backToMenuClicked(View view){
        musicService.stop();
        finish();
    }

    /**
     * Changes the sound button
     * @param view undefinded
     */
    public void changeSoundButton(View view){
        ImageButton soundButton = findViewById(R.id.sound_button);
        if(pauseSound)
            soundButton.setBackgroundResource(R.drawable.volume_off);
        else
            soundButton.setBackgroundResource(R.drawable.volume_on);
    }

    /*
    ---------------------------------------------------------------------------------------
                                         Setter & Getter
    ---------------------------------------------------------------------------------------
     */

    /**
     * Sets the current time
     * @param time new time
     */
    public void setTime(int time){this.time = time;}

    /**
     * Gets the current time
     * @return this.time
     */
    public int getTime(){return time;}

    /**
     * Gets the startTime
     * @return this.startTime
     */
    public int getStartTime(){return this.startTime;}

    /**
     * Sets the boolean {@link #lost}
     * @param lost new boolean for lost
     */
    public void setLost(boolean lost){this.lost = lost;}

    /**
     * Set a completely new level for this object
     */
    public Level setNewLevel(){this.level = new Level(this); return this.level;}

    /**
     * Returns the current level
     * @return this.level
     */
    public Level getLevel(){return this.level;}

    /**
     * Pauses the game
     * @param pauseGame sets the pause
     */
    public void setPauseGame(boolean pauseGame){
        this.pauseGame = pauseGame;
    }

    /**
     * Returns the current value for {@link #pauseGame}
     * @return this.pauseGame
     */
    public boolean getPauseGame(){return this.pauseGame;}

    /**
     * Returns this countdown object
     * @return this.countdown
     */
    public Countdown getCountdown(){return this.countdown;}

}
