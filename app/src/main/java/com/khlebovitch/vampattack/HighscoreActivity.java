package com.khlebovitch.vampattack;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Creates a new Highscore
 *
 *
 */
public class HighscoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);
    }
}
