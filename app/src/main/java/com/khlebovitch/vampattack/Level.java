package com.khlebovitch.vampattack;

/**
 * Level relevant data is stored here
 *
 *
 */
public class Level {

    /**
     * The level's name
     */
    private String name;

    /**
     * The current level of the game
     */
    private int level;

    /**
     * Creates a new default level
     */
    public Level(GameActivity gameActivity){
        this("",gameActivity);
    }

    /**
     * Creates a new level with the given name
     * @param name the level's name
     * @param gameActivity the GameActivity this object belongs to
     */
    public Level(String name, GameActivity gameActivity){
        switch (name.toUpperCase()){
            case "CASTLE":
                this.name = gameActivity.getText(R.string.castle).toString();
                break;
            default:
                this.name = gameActivity.getText(R.string.mountain).toString();
                break;
        }
        setLevel(1);
    }

    /**
     * Sets the current level
     * @param level number of level
     */
    public void setLevel(int level){this.level = level;}

    /**
     * Returns the current level
     * @return current level
     */
    public int getLevel(){
        return level;
    }

    @Override
    public String toString(){
        return name + " " + level;
    }

    /**
     * Tells how many hearts you have to get in this level
     */
    public int heartsToCollect(){
        return 5*getLevel();
    }
}
