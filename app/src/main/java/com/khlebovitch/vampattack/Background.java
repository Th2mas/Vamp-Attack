package com.khlebovitch.vampattack;
import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Holds all properties of the Background image
 */
public class Background {

    /**
     * The actual background image
     */
    private Bitmap background;

    /**
     * The background image's start x position
     */
    private int x;

    /**
     * Creates a new background with the given Bitmap
     * @param background    this background
     */
    public Background(Bitmap background){
        this.background = background;
    }

    /**
     * Updates the current background
     */
    public void update(){

        // Go to start
        if(x<(-GameView.WIDTH))
            x = 0;
    }

    /**
     * Draws this background on the given canvas
     * @param canvas Canvas to draw on
     */
    public void draw(Canvas canvas){
        canvas.drawBitmap(background,x,0,null);

        if(x<0) canvas.drawBitmap(background,x+ GameView.WIDTH,0,null);
    }
}
